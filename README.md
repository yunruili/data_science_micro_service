# Data science micro-service

> A prototypoe of end-to-end data science project: from getting the data, to training/validating the model, 
to deploying a web service for the model.

### A project directory layout

    .
    ├── models                  # Deep learning model architecture module
    ├── state_dict              # Model persistence directory
    ├── src                     # Source code (alternatively `lib` or `app`)
    ├── tests                   # Automated tests (including text cleaning, tokenization and model training pipeline unit test)
    ├── asset                   # including open API documentation..
    ├── build.sh                # Build docker image
    ├── run.sh                  # Run a command in a new container
    ├── Dockerfile              # instructions
    ├── requirements.txt        # dependencies
    └── README.md
    
### The pipeline of CI process

GitLab CI process to run the following jobs:
 - Automatically trigger
    - Coding smell, PEP8 check
    - Unit test for model training pipeline
    - Model Serving, logging, and validation requestion & response
 - Manuall trigger
    - Retrieve Data from database
    - Model Training
    
Notice:
- **We assume the model training will be trained off-line and the model won't be updated not that frequently, so I make its CI be triggered manually.**

![td-lstm](asset/ci_pipeline.jpg)

### API documentation

After the service is run, can found below in **http://127.0.0.1:5000/docs/**

![td-lstm](asset/api_documentation.jpg)

### Deployment

For this phase, due to the following two reasons:

1. Memory Limit: **image size** cannot be more than 2GB per container instance and neither be increased.
2. Compatibility: **Cloud Run Button** do not support Gitlab yet.

I did not use Googole Cloud Run to deploy the service as MVP eventually. 
However, for completeness, I add the below instruction to build image and start containger, 
which should be eaisly integrated into CD part for the future needed.

### Instuction

To demonstrate the feature, please run the below command line in order.

1. Build image

```sh
sh build.sh
```
2. Start a container

```sh
sh run.sh
```

3. Predict API

```sh
curl -H "Content-Type: application/json" -X POST -d '{"text": "grab food is my fav lah :)"}' "http://localhost:5000/predict"
```

4. API documentation 

```sh
http://127.0.0.1:5000/docs/
```

### Future work

The thing I think it can be improved if more time povided:

1. The way of model persistence: we can use google cloud storage or AWS s3 to save the serialized model instead of local file directory
2. Serving prediction performance: QPS and Model latency
