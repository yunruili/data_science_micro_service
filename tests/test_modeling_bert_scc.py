import os
import sys
import torch
from pytorch_pretrained_bert import BertModel

sys.path.append(os.path.join(os.path.dirname(__file__), '../models'))
from bert_ssc import (
    BERT_SSC,
)
from test_modeling_common import ids_tensor, Option
import unittest

class BertModelTest(unittest.TestCase):


    class BertModelTester(object):
        def __init__(
            self,
            parent,
            batch_size=16,
            seq_length=8,
            vocab_size=99,
            pretrained_bert_name = "bert-base-uncased",
            dropout = 0.1,
            bert_dim = 768,
            polarities_dim = 3,
            device = None
        ):
            self.parent = parent
            self.batch_size = batch_size
            self.seq_length = seq_length
            self.vocab_size = vocab_size
            self.polarities_dim = polarities_dim
            self.pretrained_bert_name = pretrained_bert_name
            self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu') \
                if device is None else torch.device(device)

            opt = Option()
            opt.dropout = dropout
            opt.bert_dim = bert_dim
            opt.polarities_dim = polarities_dim
            self.opt = opt

        def prepare_inputs(self):
            input_ids = ids_tensor([self.batch_size, self.seq_length], self.vocab_size, device = self.device)
            return input_ids

        def create_and_check_bert_model(
            self, input_ids
        ):
            bert = BertModel.from_pretrained(self.pretrained_bert_name)
            model = BERT_SSC(bert, self.opt).to(self.device)
            model.eval()
            # test
            self.parent.assertListEqual(list(model([input_ids]).size()), [self.batch_size, self.polarities_dim])

    def setUp(self):
        self.model_tester = BertModelTest.BertModelTester(self)

    def test_bert_scc(self):
        inputs_tensor = self.model_tester.prepare_inputs()
        self.model_tester.create_and_check_bert_model(inputs_tensor)

if __name__ == '__main__':
    unittest.main()