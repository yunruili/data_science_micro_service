import os
import sys
import numpy as np
sys.path.append(os.path.join(os.path.dirname(__file__), '../src'))
from data_utils import (
    Tokenizer4Bert,
)
import unittest

class TestTokenizer(unittest.TestCase):

    def test_pad_and_truncate(self):
        """
        Test that it can sum a list of integers
        """
        # args
        max_seq_length = 4
        pretrained_bert_name = "bert-base-uncased"
        padded_value = -1
        tokenizer = Tokenizer4Bert(max_seq_length, pretrained_bert_name)

        # test cases
        input_seq = [1,2,3]
        self.assertEqual(len(tokenizer.pad_and_truncate(input_seq, maxlen = max_seq_length)), 4)
        input_seq = [1,2,3,4,5]
        self.assertEqual(len(tokenizer.pad_and_truncate(input_seq, maxlen = max_seq_length)), 4)
        input_seq = [1,2,3]
        self.assertEqual(len(tokenizer.pad_and_truncate(input_seq, maxlen = max_seq_length)), 4)
        input_seq = [1,2,3]
        self.assertListEqual(list(tokenizer.pad_and_truncate(input_seq,max_seq_length,padding='post', truncating='post', value=0)), [1, 2, 3, 0])
        input_seq = [1,2,3]
        self.assertListEqual(list(tokenizer.pad_and_truncate(input_seq,max_seq_length,padding='pre', truncating='post', value=0)), [0, 1, 2, 3])
        input_seq = [1,2,3,4,5]
        self.assertListEqual(list(tokenizer.pad_and_truncate(input_seq,max_seq_length,padding='pre', truncating='pre', value=0)), [2, 3, 4, 5])
        input_seq = [1,2,3,4,5]
        self.assertListEqual(list(tokenizer.pad_and_truncate(input_seq,max_seq_length,padding='pre', truncating='post', value=0)), [1, 2, 3, 4])
        input_seq = [1,2]
        self.assertListEqual(list(tokenizer.pad_and_truncate(input_seq,max_seq_length,value=padded_value)), [1, 2, -1, -1])

    def test_text_to_sequence(self):
        """
        Test that it can sum a list of integers
        """
        # args
        max_seq_length = 10
        pretrained_bert_name = "bert-base-uncased"
        tokenizer = Tokenizer4Bert(max_seq_length, pretrained_bert_name)

        # test cases
        text = "grab food is my fav lah"
        text = "[CLS] " + text
        self.assertListEqual(list(tokenizer.text_to_sequence(text)), [ 101, 6723, 2833, 2003, 2026, 6904, 2615, 2474, 2232, 0])



       

if __name__ == '__main__':
    unittest.main()